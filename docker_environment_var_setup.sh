#!/usr/bin/env bash
# -*- coding: utf-8 -*-

SAMPLE_V2RAYUUID='6c3ebef2-cf90-4ee5-bd14-2bc1805eff5c'
SAMPLE_SSPASSWORD='ss@00_private'
SAMPLE_V2RAYDOMAINNAME='helloworld.example.com'

DEFAULT_SSENCRYPTION='chacha20-iet'
DEFAULT_V2RAYPATH='ws'

read -p 'Specify v2ray UUID: ' V2RAYUUID
read -p "Specify Domain name: (sample: ${SAMPLE_V2RAYDOMAINNAME})" V2RAYDOMAINNAME

rm -rf .env
touch .env
echo "V2RAYUUID=${V2RAYUUID}" >> .env
echo "V2RAYPATH=${DEFAULT_V2RAYPATH}" >> .env
echo "V2RAYDOMAINNAME=${V2RAYDOMAINNAME}" >> .env
echo "SSPASSWORD=${V2RAYUUID}" >> .env
echo "SSENCRYPTION=${DEFAULT_SSENCRYPTION}" >> .env

echo ".env file is generated."
cat .env




