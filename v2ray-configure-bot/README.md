
```bash
# test run
docker run -it --rm \
  --name v2ray-configure-bot \
  -v /home/ubuntu/lab/ray-factory/v2ray_caddy_files:/root/factory/files \
  -e V2RAYUUID=6c3ebef2-cf90-4ee5-bd14-2bc1805eff5c \
  -e V2RAYPATH=ws \
  -e V2RAYDOMAINNAME=omg.stinkycheese.club \
  v2ray-configure-bot

# debug
docker exec -it --rm \
  --name v2ray-configure-bot \
  -v "$(pwd)"/v2ray_caddy_files:/root/factory/files \
  -e V2RAYUUID=6c3ebef2-cf90-4ee5-bd14-2bc1805eff5c \
  -e V2RAYPATH=ws \
  -e V2RAYDOMAINNAME=omg.stinkycheese.club \
  v2ray-configure-bot /bin/sh
```