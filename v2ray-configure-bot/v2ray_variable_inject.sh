#!/usr/bin/env bash
# -*- coding: utf-8 -*-

# ./v2ray_variable_inject.sh \
#       $V2RAYUUID \
#       $V2RAYPATH \
#       $V2RAYDOMAINNAME

# take parameters and inject the value into the target files
# files must be placed in /root/factory/files

cd /root/factory/files

# V2RAYUUID
if [ "$1" != "" ]; then
  V2RAYUUID=$1
  echo $V2RAYUUID
  sed -i "s/<V2RAYUUID>/$V2RAYUUID/g" v2ray_tls_server.json
  sed -i "s/<V2RAYUUID>/$V2RAYUUID/g" v2ray_tls_client.json
else
  echo "V2RAYUUID is NOT provided!"
fi

# V2RAYPATH
if [ "$2" != "" ]; then
  V2RAYPATH=$2
  echo $V2RAYPATH
  sed -i "s/<V2RAYPATH>/$V2RAYPATH/g" v2ray_Caddyfile
  sed -i "s/<V2RAYPATH>/$V2RAYPATH/g" v2ray_tls_server.json
  sed -i "s/<V2RAYPATH>/$V2RAYPATH/g" v2ray_tls_client.json
else
  echo "V2RAYPATH is NOT provided!"
fi

# V2RAYDOMAINNAME
if [ "$3" != "" ]; then
  V2RAYDOMAINNAME=$3
  echo $V2RAYDOMAINNAME
  sed -i "s/<V2RAYDOMAINNAME>/$V2RAYDOMAINNAME/g" v2ray_Caddyfile
  sed -i "s/<V2RAYDOMAINNAME>/$V2RAYDOMAINNAME/g" v2ray_tls_client.json
else
  echo "V2RAYDOMAINNAME is NOT provided!"
fi
