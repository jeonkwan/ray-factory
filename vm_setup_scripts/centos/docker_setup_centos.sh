#!/usr/bin/env bash
# -*- coding: utf-8 -*-

yum install -y yum-utils \
  device-mapper-persistent-data \
  lvm2

yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo

yum install -y docker-ce docker-ce-cli containerd.io --enablerepo=docker-ce-stable

systemctl start docker
systemctl status docker
systemctl enable docker

# for non root user
#sudo usermod -aG docker $(whoami)