#!/usr/bin/env bash
# -*- coding: utf-8 -*-

read -p 'Please input non-root username: ' USERNAME

echo "Install Docker"
curl -fsSL https://get.docker.com -o get-docker.sh
sh get-docker.sh


echo "Install Docker Compose"
curl -L "https://github.com/docker/compose/releases/download/1.25.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

echo "Allow User to use Docker without sudo"
groupadd docker
usermod -aG docker $USERNAME  # username might vary

