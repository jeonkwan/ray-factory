#!/usr/bin/env bash
# -*- coding: utf-8 -*-

ufw allow ssh
ufw allow http
ufw allow https
ufw allow 60000:60010/udp
ufw allow 8990/udp
ufw allow 8990/tcp

ufw default deny incoming
ufw default allow outgoing

ufw enable
ufw status numbered