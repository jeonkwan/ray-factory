#!/usr/bin/env bash
# -*- coding: utf-8 -*-

## Install tmux and mosh
apt-get install -y tmux mosh ufw fish

## Download BBR Kernel script, execution is manual
#wget --no-check-certificate -O cd $HOME/tcp.sh https://github.com/cx9208/Linux-NetSpeed/raw/master/tcp.sh
#chmod +x tcp.sh

## Check Kernel and Boot Menu Info
#uname -r
#grep -A100 submenu  /boot/grub/grub.cfg |grep menuentry
